package com.demo.jsf.ui;

import org.apache.commons.lang3.StringUtils;

public class Login {
	
	
	
	
	private String userid;
	
	private String password;

	private boolean rememberMe;
	
	
	public boolean isRememberMe() {
		return rememberMe;
	}


	public void setRememberMe(boolean rememberMe) {
		this.rememberMe = rememberMe;
	}


	public String getUserid() {
		return userid;
	}


	public void setUserid(String userid) {
		this.userid = userid;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	
	// to navigate the user to welcome screen
    public String welcome() {
    	System.out.println("Login button clicked..");
    	
    	if(!StringUtils.isBlank(getUserid())	
    			&& !StringUtils.isBlank(getPassword())) {
    		
    		return "welcome"; //view name
    	}
    	
    	return "login";
    
    }
	
    
	
	

}
