package com.demo.jsf.ui;

public class Welcome {
	
	
	
	
	 public Welcome() {
		super();
		
		System.out.println("Welcome cons () invoked");

		
	   }

	
	   
		private String userid;
		
		private String email;
		
		private String group;
		
		private String role;
		
		private String phone;
		
		private String password;

		private String pageTitle ="JSF 2.x Welcome page";
		
		
		
		public String getPageTitle() {
			return pageTitle;
		}



		public void setPageTitle(String pageTitle) {
			this.pageTitle = pageTitle;
		}



		public String getUserid() {
			return userid;
		}



		public void setUserid(String userid) {
			this.userid = userid;
		}



		public String getEmail() {
			return email;
		}



		public void setEmail(String email) {
			this.email = email;
		}



		public String getGroup() {
			return group;
		}



		public void setGroup(String group) {
			this.group = group;
		}



		public String getRole() {
			return role;
		}



		public void setRole(String role) {
			this.role = role;
		}



		public String getPhone() {
			return phone;
		}



		public void setPhone(String phone) {
			this.phone = phone;
		}



		public String getPassword() {
			return password;
		}



		public void setPassword(String password) {
			this.password = password;
		}



		public String back() {
			return "back";
		}
		
		
}
